package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Persona;

public interface IPersonaService {

	Persona registrar(Persona persona);

	void modificar(Persona Persona);

	void eliminar(int idPersona);

	Persona listarId(int idPersona);

	List<Persona> listar();
}
